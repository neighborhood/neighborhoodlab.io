var metalsmith = require('metalsmith');
var ignore = require('metalsmith-ignore');
var sass = require('metalsmith-sass');
var serve = require('metalsmith-serve');
var watch = require('metalsmith-watch');
var path = require('path');
var pug = require('metalsmith-pug');

var metadata = require('./metadata');

var ms = metalsmith(__dirname)
  .metadata(metadata)
  .source('./source')
  .destination('./public')
  .use(ignore([
    'layouts/**',
    '**/.csscomb.json',
    '**/.csslintrc'
  ]))
  .use(sass())
  .use(pug({ locals: metadata }))

if (process.argv[2] == "serve") {
  ms.use(serve({
      host: '0.0.0.0',
      port: '5656'
    }))
    .use(watch({
      livereload: true,
      paths: {
        "${source}/**/*": '**/*.pug',
        "${source}/styles/**/*": '**/*.scss',
      }
    }))
}

ms.build(function(err) {
    if (err) console.log(err);
  });
